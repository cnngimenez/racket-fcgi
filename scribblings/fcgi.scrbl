#lang scribble/manual
@require[@for-label[racket
                    fcgi]]

@title{FastCGI for Gemini (and others?)}
@author{cnngimenez}

@defmodule[fcgi]

FastCGI for the Gemini Protocol (and maybe HTTP?)...

@hyperlink["https://gitlab.org/cnngimenez/fcgi" "Source"].

@[table-of-contents]

@section{Introduction}
This library is based on the following FastCGI documentation:

@itemlist[
@item{@url{https://www.mit.edu/~yandros/doc/specs/fcgi-spec.html}}
@item{@url{https://fastcgi-archives.github.io/}}
@item{@url{http://www.nongnu.org/fastcgi/}}]


It is possible to use some of the functions to support HTTP or other protocols.

@section{Functions}
@defproc[(start-server [fnc-record procedure?]
                       [port (and/c exact-nonnegative-integer?
                                    (integer-in 0 65535)) 4000])
                             void?]{

Start the FastCGI server.
Call @racket[fnc-record] for each request with a list of parameters and the
Output Port.
}

@defproc[(fcgi-send [outport port?]
                    [data (or/c string? bytes?)])
                    void?]{
Send a string or bytes through the FastCGI port.
}

@defproc[(read-records-from-file [filepath string?]
                                 [fnc-record void?])
                                 list?]{
Read FastCGI dumped bytes from a file and return @racket{fcgi-record} instances.

FastCGI dumps can be obtained from Wireshark or tcpdump.
}

@defstruct[fcgi-record ([fcgiversion number?]
                       [type number?]
                       [request-id number?]
                       [content-length number?]
                       [content-data bytes?])]{
Represent a record, the base unit transmitted from or to the FastCGI server or
client.
}

@defstruct[fcgi-parameter ([name-length number?]
                          [value-length number?]
                          [name-data string?]
                          [value-data string?])]{
Represent a received FastCGI parameter. Usually, these are the representation of
exported environmental variables in common CGI scripts.
}
